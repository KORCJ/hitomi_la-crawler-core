#define _crt_secure_no_warnings
#define DEBUG 0
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <atlconv.h>
#include <vector>
#include <curl/curl.h>
#include <atlbase.h>
#include <atlstr.h>
#include <io.h>
#include <locale>
#include <fstream>
#include <direct.h>
#include <pthread\pthread.h>
#include "zip.h"
#include <Windows.h>
#include <codecvt>
#include <algorithm>
#include <set>

using namespace std;

bool MAKE_THUMBNAIL		=	false,
	 IS_HTTPS_PROTOCOL	=	false,
	 MAKE_TO_ZIP		=	false;

struct PageInfo{
	wstring path, name;
	int no, prevNUM;
};

class Unicodecvt : public std::codecvt<wchar_t, char, mbstate_t>
{
protected:
	virtual bool do_always_noconv() const
	{
		return true;
	}
};

struct downloadInfoPackage{
	enum thread_stats{init = 0, running, idle};
	PageInfo *pageInfo;
	wstring *saveForlder, *table_url;
	thread_stats thread_stat;
	pthread_mutex_t *thread_stat_mutex, *cout_mutex, *tablefile_mutex;
	int my_number;

	downloadInfoPackage(){
		this->pageInfo = NULL;
		this->saveForlder = NULL;
		this->table_url = NULL;
		this->thread_stat = thread_stats::init;
		this->thread_stat_mutex = NULL;
		this->cout_mutex = NULL;
		this->tablefile_mutex = NULL;
		this->my_number = 0;
	}
	bool set(FILE *pageInfosfile, wstring *saveFolder, wstring *table_url, pthread_mutex_t *thread_stat_mutex, pthread_mutex_t *cout_mutex, pthread_mutex_t *tablefile_mutex, int my_number){

		this->pageInfo = new PageInfo();
		wchar_t *buffer = new wchar_t[300];
		fgetws(buffer, 300, pageInfosfile);
		this->pageInfo->name = buffer;
		this->pageInfo->name.pop_back();
		delete [] buffer;

		buffer = new wchar_t[100];
		fgetws(buffer, 100, pageInfosfile);
		this->pageInfo->path = buffer;
		this->pageInfo->path.pop_back();
		delete [] buffer;

		buffer = new wchar_t[100];
		fgetws(buffer, 100, pageInfosfile);
		buffer[6] = L'\0';
		this->pageInfo->no = stoi(buffer);
		delete [] buffer;

		this->saveForlder = saveFolder;
		this->table_url = table_url;
		this->thread_stat = thread_stats::running;
		this->thread_stat_mutex = thread_stat_mutex;
		this->cout_mutex = cout_mutex;
		this->tablefile_mutex = tablefile_mutex;
		this->my_number = my_number;

		return true;
	}

	void clear(){
		if (this->pageInfo != NULL){
			delete this->pageInfo;
			this->pageInfo = NULL;
		}
		this->saveForlder = NULL;
		this->table_url = NULL;
		this->thread_stat = thread_stats::init;
		this->thread_stat_mutex = NULL;
		this->cout_mutex = NULL;
		this->tablefile_mutex = NULL;
		this->my_number = 0;
	}
};

void wstring_pop_back_loop(wstring *str, int size){
	for (int i = 0; i < size;i++)
		str->pop_back();
}

void string_pop_back_loop(string *str, int size){
	for (int i = 0; i < size; i++)
		str->pop_back();
}

size_t PageWriter(void *data, size_t singleSize, size_t totalDataSize, string *downloadedData){
	if (data == NULL)
		return 0;

	downloadedData->append((char*)data, singleSize * totalDataSize);
	return singleSize*totalDataSize;
}

size_t jsFileWriter(char *data, size_t singleSize, size_t totalDataSize, string *downloadedData){
	if (data == NULL)
		return 0;

	downloadedData->append((char*) data, singleSize * totalDataSize);
	return singleSize*totalDataSize;
}

size_t imageFileWriter(void *data, size_t singleSize, size_t totalDataSize, fstream *downloadedData){
	if (data == NULL)
		return 0;
	downloadedData->write((char *)data, singleSize * totalDataSize);
	return singleSize*totalDataSize;
}

bool BeforePageinfoSorter(PageInfo first, PageInfo second){
	return first.prevNUM > second.prevNUM;
}

void *downloadFunction(void *dip){
	//PageInfo *pageinfo, string *saveFolder, string *table_url, bool *thread_stat, pthread_mutex_t *thread_stat_mute
	auto mdip = (downloadInfoPackage *) dip;
	CURL *curl;
	CURLcode res;
	curl = curl_easy_init();
	USES_CONVERSION;
	string jsPath = W2A(mdip->pageInfo->path.c_str());
	string downloadedData;
	int imageCounter = 0;

	string_pop_back_loop(&jsPath, 4);
	jsPath += "js";

	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, jsPath.c_str());
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36");
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &downloadedData);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, PageWriter);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}
	jsPath.clear();
	size_t cursor = 0;
	vector<wstring> contentNames;
	while (true){
		cursor = downloadedData.find("name", cursor);
		if (cursor++ == wstring::npos)
			break;
		cursor += 6;
		wstring contentName;
		while (downloadedData[cursor] != '\"')
			contentName += wchar_t(downloadedData[cursor++]);
		contentNames.push_back(contentName);
		imageCounter++;
	}
	downloadedData.clear();
	//ImageDownload
	pthread_mutex_lock(mdip->cout_mutex);
	wstring temp_string = mdip->pageInfo->name.c_str();
	cout << "now downloading : " << W2A(temp_string.c_str()) << endl;
	pthread_mutex_unlock(mdip->cout_mutex);
	wstring savePath = L"/";
	savePath += mdip->pageInfo->name;
	savePath += '/';
	if (_waccess(wstring(*mdip->saveForlder + L'\\' + mdip->pageInfo->name + L".zip").c_str(), 00) == -1 &&
		_waccess(wstring(*mdip->saveForlder + L'\\' + mdip->pageInfo->name).c_str(), 00) == -1 &&
		_waccess(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__/" + mdip->pageInfo->name + L".zip").c_str(), 00) == -1 &&
		_waccess(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__/" + mdip->pageInfo->name).c_str(), 00) == -1){
		_wmkdir(wstring(*mdip->saveForlder + L"\\__temp__CRAWLER__" + savePath).c_str());
	}
	else if (DEBUG){ 
		pthread_mutex_lock(mdip->cout_mutex);
		wcout << "ERROR MKDIR : " << W2A(temp_string.c_str()) << L"...and it canceled" << endl;
		pthread_mutex_unlock(mdip->cout_mutex);
		pthread_mutex_lock(mdip->thread_stat_mutex);
		mdip->thread_stat = mdip->thread_stats::idle;
		pthread_mutex_unlock(mdip->thread_stat_mutex);
		return 0; }
	else { 
		pthread_mutex_lock(mdip->cout_mutex);
		wcout << "already exist error : " << W2A(temp_string.c_str()) << L"...so it canceled" << endl;
		pthread_mutex_unlock(mdip->cout_mutex);
		pthread_mutex_lock(mdip->thread_stat_mutex);
		mdip->thread_stat = mdip->thread_stats::idle;
		pthread_mutex_unlock(mdip->thread_stat_mutex);
		return 0; }
	temp_string.clear();
	wstring_pop_back_loop(&mdip->pageInfo->path, 5);
	HZIP hz;
	if (MAKE_TO_ZIP)
		hz = CreateZip(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__/" + mdip->pageInfo->name + L".zip").c_str(), 0);
	for (int i = 1; i <= imageCounter; i++){
		fstream image(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath + contentNames[i - 1]).c_str(), ios::binary | ios::ate | ios::out);
		string download_url_string = W2A(wstring(mdip->pageInfo->path + wstring(L"/") + contentNames[i - 1]).c_str());
		curl = curl_easy_init();

		if (curl) {
			curl_easy_setopt(curl, CURLOPT_URL, download_url_string.c_str());
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36");
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &image);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, imageFileWriter);
			res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
		}
		image.close();
		download_url_string.clear();
		if (MAKE_TO_ZIP)
			ZipAdd(hz, contentNames[i - 1].c_str(), wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath + contentNames[i - 1]).c_str());

		if (MAKE_TO_ZIP && i!=1)
			DeleteFileW(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath + contentNames[i - 1]).c_str());
	}
	if (MAKE_TO_ZIP)
		CloseZip(hz);
	if (MAKE_THUMBNAIL){
		wstring filetype = L".jpg";
		if (contentNames[0].find(L".png") != wstring::npos)
			filetype = L".png";
		else if (contentNames[0].find(L".gif") != wstring::npos)
			filetype = L".gif";
		CopyFile(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath + contentNames[0]).c_str(), wstring(*mdip->saveForlder + L"/" + mdip->pageInfo->name + filetype).c_str(), true);
	}
	if (MAKE_TO_ZIP){
		MoveFile(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__/" + mdip->pageInfo->name + L".zip").c_str(), wstring(*mdip->saveForlder + L"/" + mdip->pageInfo->name + L".zip").c_str());
		DeleteFileW(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath + contentNames[0]).c_str());
	}
	else
		MoveFile(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__/" + mdip->pageInfo->name).c_str(), wstring(*mdip->saveForlder + L"/" + mdip->pageInfo->name).c_str());

	contentNames.clear();
	RemoveDirectoryW(wstring(*mdip->saveForlder + L"/__temp__CRAWLER__" + savePath).c_str());
	savePath.clear();
	pthread_mutex_lock(mdip->tablefile_mutex);
	FILE *table_file = nullptr;
	wchar_t *enterChar = new wchar_t;
	*enterChar = '\n';
	
	_wfopen_s(&table_file, mdip->table_url->c_str(), L"at, ccs=UTF-16LE");

	//no
	wstring tempNUM = to_wstring(mdip->pageInfo->no);
	fwrite(tempNUM.c_str(), sizeof(L'0'), tempNUM.length(), table_file);
	fwrite(enterChar, 2, 1, table_file);
	tempNUM.clear();

	//name
	fwrite(mdip->pageInfo->name.c_str(), sizeof(L'0'), mdip->pageInfo->name.length(), table_file);
	fwrite(enterChar, 2, 1, table_file);

	//path
	fwrite(mdip->pageInfo->path.c_str(), sizeof(L'0'), mdip->pageInfo->path.length(), table_file);
	fwrite(enterChar, 2, 1, table_file);

	//PrevNUM
	tempNUM = to_wstring(mdip->my_number);
	fwrite(tempNUM.c_str(), sizeof(L'0'), tempNUM.length(), table_file);
	fwrite(enterChar, 2, 1, table_file);
	tempNUM.clear();
	
	fclose(table_file);
	delete enterChar;
	pthread_mutex_unlock(mdip->tablefile_mutex);

	pthread_mutex_lock(mdip->thread_stat_mutex);
	mdip->thread_stat = mdip->thread_stats::idle;
	pthread_mutex_unlock(mdip->thread_stat_mutex);

	return 0;
}

int c_str_find(char *str, char const *dst, size_t pos = 0){
	int str_len = strlen(str), dst_len = strlen(dst);
	for (bool check = true; check && pos < str_len; pos++, check = true){
		for (int i = 0; i < dst_len; i++){
			if (str[pos + i] != dst[i]){
				check = false;
				return pos;
			}
		}
	}
	return -1;
}

int get_recent_page_info(FILE *table, set<int> *_set, wstring *prev_format){

	//first, read prev format
	wchar_t *buffer = new wchar_t[100];
	fgetws(buffer, 100, table);
	*prev_format = buffer;
	prev_format->pop_back();
	delete [] buffer;
	
	int _max_finder = 0, end_time = 0;

	while (true){
		//no
		buffer = new wchar_t[100];
		memset(buffer, 0, sizeof(buffer));
		fgetws(buffer, 100, table);
		if (buffer[0] == 0)
			return _max_finder;
		_set->insert(_wtoi(buffer));
		if (_set->size() > 25)
			_set->erase(_set->begin());
		delete [] buffer;

		//name
		buffer = new wchar_t[100];
		memset(buffer, 0, sizeof(buffer));
		fgetws(buffer, 100, table);
		delete [] buffer;

		//path
		buffer = new wchar_t[100];
		memset(buffer, 0, sizeof(buffer));
		fgetws(buffer, 100, table);
		delete [] buffer;

		//PrevNUM
		buffer = new wchar_t[100];
		memset(buffer, 0, sizeof(buffer));
		fgetws(buffer, 100, table);

		//find mex num
		if (_wtoi(buffer) > _max_finder){
			_max_finder = _wtoi(buffer);
		}

		delete [] buffer;
	}
	return _max_finder;
}

void adjust_to_able_name(wstring *name){
	for (int i = 0; i < name->size(); i++){
		switch (name->at(i)){
		case L'\\':
		case L'/':
		case L':':
		case L'*':
		case L'?':
		case L'\"':
		case L'<':
		case L'>':
		case L'|': name->erase(i, 1); break;
		}
	}
}

int main(int argc, char* argv [])
{
//	_CrtSetBreakAlloc(1670);

	setlocale(LC_ALL, "UTF-16");
	//make option
	int _select_limit_option, limit = 0, thread_limit, db_counter = 0, end_time;
	wifstream table_file;
	wstring table_url, save_url, mainFrame_url, prev_format, temp;
	bool format_checking = true, ticket = true;
	set<int> prevSetTable;
	wcout << "core v1.029" << endl;
	//stand-alone type
	if (argc == 1){
		cout << "this programem no more support stand-alone mode." << endl;
		system("PAUSE");
		return -1;
		/*
		while (ticket){
		cout << "set limit option " << endl << "1 : with pageNumber" << endl << "2 : use table" << endl << "select : ";
		cin >> _select_limit_option;
		if (_select_limit_option == 1){
		cout << "limit? : ";
		cin >> limit;
		while (true){
		cout << "Please Input hitomi.la's url" << endl;
		cout << "EX) http://hitomi.la/index-korean-[NUM].html" << endl;
		cin >> mainFrame_url;
		if (_select_limit_option == 2 && prev_format != mainFrame_url){
		cout << "matching failed with table's url format." << endl;
		cout << "if you want make new table and continue, press anykey with enter." << endl;
		getchar();
		format_checking = false;
		}
		int tempCursor = mainFrame_url.find("[NUM]");
		string_pop_back_loop(&mainFrame_url, mainFrame_url.size() - tempCursor);
		ticket = false;
		break;
		}
		}
		else if (_select_limit_option == 2){
		bool pass = false;
		while (true){
		cout << "Please Input URL For Table" << endl << "table_location : ";
		cin >> table_url;
		cout << "read from table file....." << endl;
		try{
		table_file.open(table_url);
		//					table_file >> limit;
		db_counter = get_recent_page_info(&table_file, &prevInfos, &prev_format, &end_time);
		table_file.close();
		string retake_table_url = table_url.c_str();

		//					rename(table_url.c_str(), )
		cout << "Succeed!" << endl;
		cout << "WARNING : DO NOT TOUCH THIS TABLEFILE" << endl;
		pass = true;
		mainFrame_url = prev_format;
		int tempCursor = mainFrame_url.find("[NUM]");
		string_pop_back_loop(&mainFrame_url, mainFrame_url.size() - tempCursor);
		break;
		}
		catch (exception){
		cout << "Can't open table file, try again." << "-----------------------------------" << endl << endl << endl;
		}
		}
		if (pass)
		break;
		}
		else{
		cout << "wrong number, try again." << "-----------------------------------" << endl << endl << endl;
		}
		}
		if (_select_limit_option == 1 || !format_checking){
		if (!format_checking){
		cout << "url format doesn't matching!, please use another path" << endl;
		}
		cout << "Please Input URL For SAVE!!!!" << endl;
		cout << "SaveFolder : " << endl;
		cin >> save_url;
		table_url = save_url;
		table_url += '\\';
		table_url += "TABLE.table";
		fstream table_file(table_url, ios::out | ios::app);
		table_file << mainFrame_url << "[NUM].html";
		table_file.close();
		}
		else{
		save_url = table_url;
		string_pop_back_loop(&save_url, 12);
		cout << "SaveFolder is " << save_url << endl;
		}
		cout << "Please set multy_download session : ";
		cin >> thread_limit;
		*/
	}
	//with GUI type
	else{
		USES_CONVERSION;
		while (ticket){
			_select_limit_option = *argv[1] - '0';
			if (_select_limit_option == 1){
				limit = stoi(argv[2]);
				mainFrame_url = A2W(argv[3]);
				int tempCursor = mainFrame_url.find(L"[NUM]");
				wstring_pop_back_loop(&mainFrame_url, mainFrame_url.size() - tempCursor);
				ticket = false;
			}
			else if (_select_limit_option == 2){
				bool pass = false;
				while (true){
					table_url = A2W(argv[2]);
					try{
						FILE *table_file = nullptr;
						_wfopen_s(&table_file, table_url.c_str(), L"rt, ccs=UTF-16LE");
						db_counter = get_recent_page_info(table_file, &prevSetTable, &prev_format);
						fclose(table_file);
						end_time = *prevSetTable.begin();
						wstring retake_table_url = table_url.c_str();

						wcout << "WARNING : DO NOT TOUCH TABLEFILE" << endl;
						pass = true;
						mainFrame_url = prev_format;
						int tempCursor = mainFrame_url.find(L"[NUM]");
						wstring_pop_back_loop(&mainFrame_url, mainFrame_url.size() - tempCursor);
						retake_table_url.clear();
						break;
					}
					catch (exception){
						wcout << "Can't open table file, try again." << "-----------------------------------" << endl << endl << endl;
					}
				}
				if (pass)
					break;
			}
		}
		if (_select_limit_option == 1){
			save_url = A2W(argv[4]);
			table_url = save_url;
			table_url += L'\\';
			table_url += L"TABLE.table";
			std::wofstream table_file(table_url, std::ios_base::binary);
			table_file.imbue(std::locale(std::locale(""), new Unicodecvt));
			table_file << wchar_t(0xFEFF);
			table_file << mainFrame_url.c_str() << L"[NUM].html";
			table_file << L'\n';
			table_file.close();
		}
		else{
			save_url = table_url;
			wstring_pop_back_loop(&save_url, 12);
			wcout << "SaveFolder is " << save_url << endl;
		}
		if (_select_limit_option == 1){
			thread_limit = stoi(argv[5]);
			MAKE_THUMBNAIL = (stoi(argv[6]) == 1);
			MAKE_TO_ZIP = (stoi(argv[7]) == 1);
		}
		else{
			thread_limit = stoi(argv[3]);
			MAKE_THUMBNAIL = (stoi(argv[4]) == 1);
			MAKE_TO_ZIP = (stoi(argv[5]) == 1);
		}
	}

	//set end-time
	//set "set" table for info

	wcout << "ALL INFORMATION HAS ACCPETED." << endl << endl;
	wcout << "now, download the information for the files" << endl;
	{
		USES_CONVERSION;
		if (_waccess(wstring(wstring(save_url.c_str()) + L"\\__temp__CRAWLER__\\").c_str(), 00) != -1){
			HANDLE hnd, hnd2;
			WIN32_FIND_DATAW fdata, fdata2;
			wstring aa = wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\*");
			hnd = FindFirstFileW(aa.c_str(), &fdata);
			if (hnd != INVALID_HANDLE_VALUE){
				do{
					if ((fdata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY){
						wstring tempPath = wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName);
						SetFileAttributes(tempPath.c_str(), FILE_ATTRIBUTE_NORMAL);
						DeleteFile(tempPath.c_str());
					}
					else{
						if (fdata.cFileName[0] != L'.'){
							hnd2 = FindFirstFileW(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName + L"\\*").c_str(), &fdata2);
							if (hnd2 != INVALID_HANDLE_VALUE){
								do{
									if (fdata2.cFileName[0] != L'.'){
										wstring tempPath = wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName + L"\\" + fdata2.cFileName);
										SetFileAttributes(tempPath.c_str(), FILE_ATTRIBUTE_NORMAL);
										DeleteFile(tempPath.c_str());
									}
								} while (FindNextFileW(hnd2, &fdata2) == TRUE);
								FindClose(hnd2);
								RemoveDirectory(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str());
							}
							SetFileAttributes(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str(), FILE_ATTRIBUTE_NORMAL);
							RemoveDirectory(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str());
						}
					}
				} while (FindNextFileW(hnd, &fdata) == TRUE);
			}
			FindClose(hnd);
			SetFileAttributes(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\").c_str(), FILE_ATTRIBUTE_NORMAL);
			RemoveDirectory(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\").c_str());
		}
		_wmkdir(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\").c_str());
	}

	CURL *curl;
	CURLcode res;
	string listPageUrl, downloadedData;
	int pageInfos = 0;
	bool ended = false;
	wfstream pageInfosfile;

	///
	/// read From Page
	///

	std::wofstream fs(wstring(wstring((save_url.c_str())) + L"\\__temp__CRAWLER__\\PAGEINFOS.txt"), std::ios_base::binary);
	fs.imbue(std::locale(std::locale(""), new Unicodecvt));
	fs << wchar_t(0xFEFF);
	fs.close();
	IS_HTTPS_PROTOCOL = (mainFrame_url.find(L"https://") != wstring::npos);

	//if type is 2, find where is limit
	//make page list
	if (_select_limit_option == 2){
		cout << "...searching start position..." << endl;
		ended = false;
		int tempLIMIT = 0;
		while (!ended){
			listPageUrl.clear();
			USES_CONVERSION;
			listPageUrl = string(W2A(mainFrame_url.c_str()) + to_string(++tempLIMIT) + ".html");
			curl = curl_easy_init();
			if (curl) {
				curl_easy_setopt(curl, CURLOPT_URL, listPageUrl.c_str());
				curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36");
				curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, &downloadedData);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, PageWriter);
				res = curl_easy_perform(curl);
				curl_easy_cleanup(curl);
			}
			size_t cursor = 0, prev_cursor = 1;
			int last_checker = 0;
			while (true){
				cursor = downloadedData.find("</a>\n<h1><a href=\"/galleries/", cursor);
				//no more
				if (cursor == string::npos)
					break;
				prev_cursor = cursor;
				last_checker++;
				///succeed find
				cursor += sizeof("</a>\n<h1><a href=\"/galleries/");
				cursor--;
				//find file no
				string tempString;
				while (downloadedData[cursor] != '.')
					tempString += downloadedData[cursor++];
				if (stoi(tempString) == end_time){
					limit = tempLIMIT;
					ended = true;
					break;
				}
			}
			if (last_checker == 0)
				break;
			downloadedData.clear();
		}
	}

	ended = false;
	vector<PageInfo> tempPGinfo;
	while (!ended){
		tempPGinfo.clear();
		if (limit < 1)
			break;
		listPageUrl.clear();
		USES_CONVERSION;
		listPageUrl = string(W2A(mainFrame_url.c_str()) + to_string(limit) + ".html");
		cout << "now reading page : " << limit;
		curl = curl_easy_init();
		if (curl) {
			curl_easy_setopt(curl, CURLOPT_URL, listPageUrl.c_str());
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36");
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &downloadedData);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, PageWriter);
			res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
		}
		if (DEBUG == 1){
			ofstream listfile("OUTPUT.txt");
			listfile << downloadedData;
			listfile.close();
		}
		//make page list
		size_t cursor = 0, prev_cursor = 1;
		int last_checker = 0;
		while (true){
			cursor = downloadedData.find("</a>\n<h1><a href=\"/galleries/", cursor);
			//no more
			if (cursor == string::npos || cursor < prev_cursor)
				break;
			prev_cursor = cursor;
			last_checker++;
			///succeed find
			cursor += sizeof("</a>\n<h1><a href=\"/galleries/");
			cursor--;
			PageInfo pginfo;
			//find file no
			string tempString;
			while (downloadedData[cursor] != '.')
				tempString += downloadedData[cursor++];
			pginfo.no = stoi(tempString);
			if (_select_limit_option == 2){
				if (end_time > pginfo.no || prevSetTable.end() != prevSetTable.find(pginfo.no))
					continue;
			}
			//find file name
			cursor += string(".html\">").size();
			tempString.clear();
			while (downloadedData[cursor] != '<')
				tempString += downloadedData[cursor++];
			//convert to UTF-8
			USES_CONVERSION;
			char *tempUTF8 = new char[tempString.size() + 1];
			int size = 0, caps = 0;
			for (int i = 0; i < tempString.size(); i++, caps = 0){
				tempUTF8[i] = tempString[i];

				caps = tempString[i];
				if (caps < 0)
					caps = caps ^ 0xffffff00;
				else{
					//it's ascii-word
					size++;
					continue;
				}
				//it's 3-word?
				caps = caps >> 4;
				if (caps == 0xe){
					size++;
					continue;
				}
				//it's 2-word?
				caps = caps >> 1;
				if (caps == 0x6){
					size++;
					continue;
				}
				//it's just left-word. drop
			}
			wchar_t *tempUnicode = new wchar_t[size+2];
			tempUTF8[tempString.size()] = '\0';
			MultiByteToWideChar(CP_UTF8, 0, tempUTF8, -1, tempUnicode, size+1);
			pginfo.name = tempUnicode;
			adjust_to_able_name(&pginfo.name);
			pginfo.path = wstring((IS_HTTPS_PROTOCOL) ? L"https://hitomi.la/galleries/" : L"http://hitomi.la/galleries/") + to_wstring(pginfo.no) + wstring(L".html");
			pageInfos++;
			delete [] tempUnicode;
			delete [] tempUTF8;
			tempPGinfo.push_back(pginfo);

			if (last_checker == 0)
				break;
		}
		FILE *pageInfosfile = nullptr;
		wchar_t *enterChar = new wchar_t;
		*enterChar = '\n';
		//for multy language
		_wfopen_s(&pageInfosfile, wstring(save_url + L"\\__temp__CRAWLER__\\PAGEINFOS.txt").c_str(), L"at, ccs=UTF-16LE");
		for (int i= tempPGinfo.size() - 1; i >= 0;i--){
			//filename
			fwrite(tempPGinfo[i].name.c_str(), sizeof(tempPGinfo[i].name[0]), tempPGinfo[i].name.size(), pageInfosfile);
			fwrite(enterChar, 2, 1, pageInfosfile);

			//path
			fwrite(tempPGinfo[i].path.c_str(), sizeof(tempPGinfo[i].path[0]), tempPGinfo[i].path.length(), pageInfosfile);
			fwrite(enterChar, 2, 1, pageInfosfile);

			//no
			wstring tempNO = to_wstring(tempPGinfo[i].no);
			fwrite(tempNO.c_str(), sizeof(tempNO[0]), tempNO.length(), pageInfosfile);
			fwrite(enterChar, 2, 1, pageInfosfile);
			tempNO.clear();
		}
		fclose(pageInfosfile);
		tempPGinfo.clear();
		delete [] enterChar;

		downloadedData.clear();
		wcout << L" ... done!" << endl;
		limit--;
		Sleep(0.1);
	}
	//clearing
	tempPGinfo.clear();
	mainFrame_url.clear();
	prev_format.clear();
	listPageUrl.clear();
	prevSetTable.clear();
	temp.clear();

	wstring w_table_url, w_save_url;
	{
		USES_CONVERSION;
		w_table_url =  (table_url.c_str());
		table_url.clear();
		w_save_url =  (save_url.c_str());
		save_url.clear();
	}

	//downlaod file
	cout << "now, download the files" << endl;
//	pageInfos = 2, thread_limit = 1;

	int counter = pageInfos - 1, thread_counter = 0, running_checker = 0;
	if (thread_limit > pageInfos)
		thread_limit = pageInfos;
	pthread_t *threads = new pthread_t[thread_limit];
	downloadInfoPackage *dnInfoPkg = new downloadInfoPackage[thread_limit];
	pthread_mutex_t thread_stat_mutex = PTHREAD_MUTEX_INITIALIZER,
					cout_mutex = PTHREAD_MUTEX_INITIALIZER,
					tablefile_mutex = PTHREAD_MUTEX_INITIALIZER;
	db_counter++;

	FILE *REpageInfosfile = nullptr;
	_wfopen_s(&REpageInfosfile, wstring(w_save_url + L"\\__temp__CRAWLER__\\PAGEINFOS.txt").c_str(), L"rt, ccs=UTF-16LE");
	int return_value;
	while (true){
		running_checker = 0;
		for (int i = 0; i < thread_limit; i++){
			pthread_mutex_lock(&thread_stat_mutex);
			if (counter >= 0 && (dnInfoPkg[i].thread_stat == downloadInfoPackage::thread_stats::init || dnInfoPkg[i].thread_stat == downloadInfoPackage::thread_stats::idle)){
				if (DEBUG == 1){
					pthread_mutex_lock(&cout_mutex);
					cout << "now in thread " << i << ". counter : " << counter <<endl;
					pthread_mutex_unlock(&cout_mutex);
				}
				if (dnInfoPkg[i].thread_stat == downloadInfoPackage::thread_stats::idle){
					pthread_join(threads[i], (void**)&return_value);
				}
				dnInfoPkg[i].clear();
				if (dnInfoPkg[i].set(REpageInfosfile, &w_save_url, &w_table_url, &thread_stat_mutex, &cout_mutex, &tablefile_mutex, db_counter++)){
					pthread_create(&threads[i], NULL, &downloadFunction, &dnInfoPkg[i]);
					counter--;
				}
				else{
					pthread_mutex_unlock(&thread_stat_mutex);
					counter--;
					continue;
				}
			}
			else if (dnInfoPkg[i].thread_stat == downloadInfoPackage::thread_stats::idle){
				dnInfoPkg[i].clear();
			}
			if (dnInfoPkg[i].thread_stat == downloadInfoPackage::thread_stats::running){
				running_checker++;
			}
			pthread_mutex_unlock(&thread_stat_mutex);
		}
		if (running_checker == 0 && counter < 0){
			break;
		}
	}
	fclose(REpageInfosfile);
	delete [] threads;
	delete [] dnInfoPkg;
	w_save_url.clear();
	w_table_url.clear();
	pthread_mutex_destroy(&thread_stat_mutex);
	pthread_mutex_destroy(&cout_mutex);
	pthread_mutex_destroy(&tablefile_mutex);
	//FINAL CLEAN UP
	{
		USES_CONVERSION;
		if (_waccess(wstring(w_save_url + L"\\__temp__CRAWLER__\\").c_str(), 00) != -1){
			HANDLE hnd, hnd2;
			WIN32_FIND_DATAW fdata, fdata2;
			wstring aa = wstring(w_save_url + L"\\__temp__CRAWLER__\\*");
			hnd = FindFirstFileW(aa.c_str(), &fdata);
			if (hnd != INVALID_HANDLE_VALUE){
				do{
					if ((fdata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY){
						wstring tempPath = wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName);
						SetFileAttributes(tempPath.c_str(), FILE_ATTRIBUTE_NORMAL);
						DeleteFile(tempPath.c_str());
					}
					else{
						if (fdata.cFileName[0] != L'.'){
							hnd2 = FindFirstFileW(wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName + L"\\*").c_str(), &fdata2);
							if (hnd2 != INVALID_HANDLE_VALUE){
								do{
									if (fdata2.cFileName[0] != L'.'){
										wstring tempPath = wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName + L"\\" + fdata2.cFileName);
										SetFileAttributes(tempPath.c_str(), FILE_ATTRIBUTE_NORMAL);
										DeleteFile(tempPath.c_str());
									}
								} while (FindNextFileW(hnd2, &fdata2) == TRUE);
								FindClose(hnd2);
								RemoveDirectory(wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str());
							}
							SetFileAttributes(wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str(), FILE_ATTRIBUTE_NORMAL);
							RemoveDirectory(wstring(w_save_url + L"\\__temp__CRAWLER__\\" + fdata.cFileName).c_str());
						}
					}
				} while (FindNextFileW(hnd, &fdata) == TRUE);
			}
			FindClose(hnd);
			SetFileAttributes(wstring(w_save_url + L"\\__temp__CRAWLER__\\").c_str(), FILE_ATTRIBUTE_NORMAL);
			RemoveDirectory(wstring(w_save_url + L"\\__temp__CRAWLER__\\").c_str());
		}
		_wmkdir(wstring(w_save_url + L"\\__temp__CRAWLER__\\").c_str());
	}
	cout << "ALL WORK HAS BEEN DONE" << endl;
	system("PAUSE");
	_CrtDumpMemoryLeaks();
	return 0;
}